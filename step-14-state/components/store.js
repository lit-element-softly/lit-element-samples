/*
  - https://www.smashingmagazine.com/2018/07/redux-designers-guide/
  - http://ramonvictor.github.io/tic-tac-toe-js/

*/
// Import redux store logic
//import { createStore } from 'redux'
/*
  <script>
    // Redux assumes `process.env.NODE_ENV` exists in the ES module build.
    // https://github.com/reactjs/redux/issues/2907
    // window.process = { env: { NODE_ENV: 'production' } }
  </script>
*/

import { createStore } from '../js/redux.js'

// reducer
function counterReducer(state = 0, action) {
  switch (action.type) {
    case 'INCREMENT':
      return state + 1
    case 'DECREMENT':
      return state - 1
    default:
      return state
  }
}

// Create and export store so it can be imported and shared by app elements
export const store = createStore(counterReducer)