import { LitElement, html } from '../js/lit-element.js'

import {MyTitle} from './my-title.js'
import {MyCounter} from './my-counter.js'
import {IncrementButton} from './increment-button.js'
import {DecrementButton} from './decrement-button.js'

export class MainApplication extends LitElement {

  static get styles() { return [window.picnicCss, window.fineTuningCss] }

  render() {
    return html`

      <section class="section">
        <div class="container has-text-centered">
          <my-title></my-title>
        </div>
      </section>

      <section class="section">
        <div class="container has-text-centered">
          <my-counter></my-counter>
        </div>
      </section>

      <section class="section">
        <div class="container has-text-centered">
          <decrement-button></decrement-button>
          <increment-button></increment-button>
        </div>
      </section>
    `
  }
}

customElements.define('main-application', MainApplication)
