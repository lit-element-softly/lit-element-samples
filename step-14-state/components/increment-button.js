import { LitElement, html } from '../js/lit-element.js'

// 👋 this is the store
import { store } from './store.js'

export class IncrementButton extends LitElement {
  static get styles() { return [window.picnicCss, window.fineTuningCss] }

  render(){
    return html`
      <button @click="${this.onClick}">
        😃 Increment
      </button>
    `
  }

  onClick() {
    store.dispatch({ type: 'INCREMENT' })
  }
}
customElements.define('increment-button', IncrementButton)