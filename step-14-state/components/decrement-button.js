import { LitElement, html } from '../js/lit-element.js'

// 👋 this is the store
import { store } from './store.js'

export class DecrementButton extends LitElement {
  static get styles() { return [window.picnicCss, window.fineTuningCss] }

  render(){
    return html`
      <button @click="${this.onClick}">
        😠 Decrement
      </button>
    `
  }

  onClick() {
    store.dispatch({ type: 'DECREMENT' })
  }
}
customElements.define('decrement-button', DecrementButton)