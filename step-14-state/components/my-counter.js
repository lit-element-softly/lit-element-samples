import { LitElement, html } from '../js/lit-element.js'

// 👋 this is the store
import { store } from './store.js'

export class MyCounter extends LitElement {

  static get styles() { return [window.picnicCss, window.fineTuningCss] }

  static get properties(){
    return {
      counter: Number
    }
  }

  render(){

    return html`
      <h1>
        ${this.counter}
      </h1> 
    `
  }
  // initialize at first change
  firstUpdated(changedProperties) {
    this.counter = store.getState()
    store.subscribe(() => {
      this.counter = store.getState()
    })
  }

}
customElements.define('my-counter', MyCounter)