import { LitElement, html } from '../js/lit-element.js'
import {MyTitle} from './my-title.js';

export class MainApplication extends LitElement {
  
  render() {
    return html`
      <link rel="stylesheet" href="../css/amazing.css" />
      <section class="container">
        <my-title></my-title>
      <section>     
    `
  }
}

customElements.define('main-application', MainApplication)
