import { LitElement, html } from '../js/lit-element.js'
import { installRouter } from '../js/pwa-helpers.js'

import { store } from './store.js'


export class MessagesList extends LitElement {

  static get properties(){
    return {
      counter: Number
    }
  }

  static get properties() {
    return {
      messages: { type: Array }
    }
  }
  
  constructor() {
    super()
    window.store = store
    store.dispatch({ type: 'INIT' })
    this.messages = store.getState()
    console.log(this.messages)
    store.subscribe(() => {
      this.messages = store.getState()
    })
    /*
    this.messages = [
      {avatar: "🦊", name: "Foxy"},
      {avatar: "🦁", name: "Leo"},
      {avatar: "🐯", name: "Tigrou"}
    ]
    */
  }
  
  render() {
    return html`
      <table>
        <thead>
          <tr><th>message</th></tr>
        </thead>
        <tbody>
          ${this.messages.map(message => 
            html`
              <tr>
                <td>${message}</td>
              </tr>
            `
          )}
        </tbody>
      </table>
    `
  }

}

customElements.define('messages-list', MessagesList)
