import { LitElement, html } from '../js/lit-element.js'
import { installRouter } from '../js/pwa-helpers.js'


import {MyTitle} from './my-title.js'
import {MySubTitle} from './my-sub-title.js'
import {MyAmazingButton} from './my-amazing-button.js'
import {BuddiesList} from './buddies-list.js'
import {MyField} from './my-field.js'
import {BuddyForm} from './buddy-form.js'


import {TitleOne} from './title-one.js'
import {TitleTwo} from './title-two.js'
import {TitleThree} from './title-three.js'

// https://k33g.gitlab.io/articles/2019-01-13-LITELEMENT-04.html

export class MainApplication extends LitElement {

  static get styles() { return [window.picnicCss, window.fineTuningCss] }

  static get properties() {
    return {
      subComponent: { type: Object }
    }
  }

  constructor() {
    super()

    // the main application is listening
    this.addEventListener('add-buddy', this.onMessage)

    // router demo
    this.subComponent = html`<title-one></title-one>`

    installRouter((location) => {
      console.log(location)
      switch (location.hash) {
        case "#display/one":
          this.subComponent = html`<title-one></title-one>`
          break
        case "#display/two":
          this.subComponent = html`<title-two></title-two>`
          break
        case "#display/three":
          this.subComponent = html`<title-three></title-three>`
          break
        default:
          break
      }
    })




  }

  onMessage(message) {
    if(message.type==='add-buddy') {
      console.log(message.detail)
      this.shadowRoot.querySelector('buddies-list').addBuddy(message.detail)
    }    
  }


  render() {
    return html`
      <article class="card" style="padding: 10px;">
        <header>
          <my-title></my-title>
        </header>
        <my-sub-title my-text="I ❤️ LitElement"></my-sub-title>
      </article>

      <article class="card" style="padding: 10px;">
        <header>
          <a class="tag is-large is-dark" href="/#display/one">one</a> 
          <a class="tag is-large is-link" href="/#display/two">two</a>
          <a class="tag is-large is-primary" href="/#display/three">three</a>
        </header>
        ${this.subComponent}
      </article>



      <article class="card" style="padding: 10px;">
        <header>
          type something
        </header>
        <my-field></my-field>
      </article>

      <article class="card" style="padding: 10px;">
        <header>
          Add a buddy
        </header>
        <buddy-form></buddy-form>
      </article>

      <article class="card" style="padding: 10px;">
        <header>
          Buddies
        </header>
        <buddies-list></buddies-list>
      </article>


      <article class="card" style="padding: 10px;">
        <header>
          My Buttons
        </header>
        <my-amazing-button></my-amazing-button>
        <my-amazing-button></my-amazing-button>
      </article>

    `
  }
}

customElements.define('main-application', MainApplication)
