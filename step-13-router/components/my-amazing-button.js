import { LitElement, html } from '../js/lit-element.js'

export class MyAmazingButton extends LitElement {

  static get styles() { return [window.picnicCss, window.fineTuningCss] }

  static get properties() {
    return {
      counter: { type: Number }
    }
  }
  
  constructor() {
    super()
    this.counter = 0
  }
  
  render(){
    return html`
      <button @click="${this.onClick}">
        👋 Click me! ${this.counter}
      </button>
    `
  }

  onClick() {
    this.counter+=1
  }
}
customElements.define('my-amazing-button', MyAmazingButton)