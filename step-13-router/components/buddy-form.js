import { LitElement, html } from '../js/lit-element.js'

export class BuddyForm extends LitElement {
  
  static get styles() { return [window.picnicCss, window.fineTuningCss] }

  render(){

    return html`

      <fieldset class="flex two">
        <label>
          <input 
            id="avatar"
            type="text" 
            placeholder="🎃">
        </label>
        <label>
          <input 
            id="name"
            type="text" 
            placeholder="John Doe">          
        </label>   
        <label>
          <button @click="${this.onClick}">
            Add buddy
          </button>
        </label>   
      </fieldset>
    `
  }

  get avatar() {
    return this.shadowRoot.getElementById('avatar').value
  }

  get name() {
    return this.shadowRoot.getElementById('name').value
  }

  /*
  set avatar(value) {
    this.shadowRoot.getElementById('avatar').value = value
  }

  set name(value) {
    this.shadowRoot.getElementById('name').value = value
  }
  */


  onClick() {
    this.dispatchEvent(
      new CustomEvent('add-buddy', {  
        bubbles: true, composed: true, detail: {
          name: this.name, avatar: this.avatar
        } 
      })
    )
    this.avatar = ""
    this.name = ""
  }

}
customElements.define('buddy-form', BuddyForm)