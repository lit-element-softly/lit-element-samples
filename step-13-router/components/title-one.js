import { LitElement, html } from '../js/lit-element.js'


export class TitleOne extends LitElement {
  static get styles() { return [window.picnicCss, window.fineTuningCss] }

  render(){
    return html`
      <h1>One</h1>
    `
  }
}
customElements.define('title-one', TitleOne)
