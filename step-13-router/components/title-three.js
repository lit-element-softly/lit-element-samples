import { LitElement, html } from '../js/lit-element.js'


export class TitleThree extends LitElement {
  static get styles() { return [window.picnicCss, window.fineTuningCss] }

  render(){
    return html`
      <h3>Three</h3>
    `
  }
}
customElements.define('title-three', TitleThree)
