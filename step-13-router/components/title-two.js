import { LitElement, html } from '../js/lit-element.js'


export class TitleTwo extends LitElement {
  static get styles() { return [window.picnicCss, window.fineTuningCss] }

  render(){
    return html`
      <h2>Two</h2>
    `
  }
}
customElements.define('title-two', TitleTwo)
