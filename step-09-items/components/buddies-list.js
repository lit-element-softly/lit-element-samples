import { LitElement, html } from '../js/lit-element.js'

export class BuddiesList extends LitElement {

  static get styles() { return [window.picnicCss, window.fineTuningCss] }

  static get properties() {
    return {
      buddies: { type: Array }
    }
  }

  constructor() {
    super()
    this.buddies = [
      {avatar: "🦊", name: "Foxy"},
      {avatar: "🦁", name: "Leo"},
      {avatar: "🐯", name: "Tigrou"}
    ]
  }

  render(){
    return html`
      <table class="primary">
        <thead>
          <tr><th>avatar</th><th>name</th></tr>
        </thead>
        <tbody>
          ${this.buddies.map(buddy => 
            html`
              <tr>
                <td>${buddy.avatar}</td>
                <td>${buddy.name}</td>
              </tr>
            `
          )}
        </tbody>
      </table>
    `
  }
}
customElements.define('buddies-list', BuddiesList)

