import { LitElement, html } from '../js/lit-element.js'

import {MyTitle} from './my-title.js'
import {MySubTitle} from './my-sub-title.js'
import {MyAmazingButton} from './my-amazing-button.js'
import {BuddiesList} from './buddies-list.js'
import {MyField} from './my-field.js'


export class MainApplication extends LitElement {

  static get styles() { return [window.picnicCss, window.fineTuningCss] }

  render() {
    return html`

      <article class="card" style="padding: 10px;">
        <header>
          <my-title></my-title>
        </header>
        <my-sub-title my-text="I ❤️ LitElement"></my-sub-title>
      </article>

      <!--
        <div class="container has-text-centered">
          regarder dans spectre
      -->
      <article class="card" style="padding: 10px;">
        <header>
          type something
        </header>
        <my-field></my-field>
      </article>


      <article class="card" style="padding: 10px;">
        <header>
          Buddies
        </header>
        <buddies-list></buddies-list>
      </article>


      <article class="card" style="padding: 10px;">
        <header>
          My Buttons
        </header>
        <my-amazing-button></my-amazing-button>
        <my-amazing-button></my-amazing-button>
      </article>
    `
  }
}

customElements.define('main-application', MainApplication)
