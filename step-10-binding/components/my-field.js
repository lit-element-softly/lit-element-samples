import { LitElement, html } from '../js/lit-element.js'


export class MyField extends LitElement {

  static get styles() { return [window.picnicCss, window.fineTuningCss] }

  render(){
    return html`
    <div>
      <fieldset class="flex two">
        <label>
          <input 
            id="my_field"
            type="text" placeholder="👋 Hello World 🌍"
            @input=${
              () =>
                this.shadowRoot.querySelector('p').innerHTML 
                  = this.shadowRoot.querySelector('input').value
            }
          >
        </label>        
      </fieldset>
      <p style="display: block;">This is a help text</p>

    </div>
    `
  }

}
customElements.define('my-field', MyField)



