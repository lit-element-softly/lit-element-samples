import { LitElement, html } from '../js/lit-element.js'

export class MyTitle extends LitElement {

  static get styles() { return [window.picnicCss, window.fineTuningCss] }

  render(){
    return html`
      <h1>🖖 Hey 🌍</h1>
    `
  }
}
customElements.define('my-title', MyTitle)