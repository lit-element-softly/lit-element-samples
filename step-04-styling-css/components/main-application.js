import { LitElement, html } from '../js/lit-element.js'
import {MyTitle} from './my-title.js'

export class MainApplication extends LitElement {

  static get styles() { return [window.picnicCss] }

  constructor() {
    super()
  }

  render() {
    return html`
      <section>
        <my-title></my-title>
        <my-title></my-title>

        <a class="tag is-large is-dark" href="/#display/one">one</a> 
        <a class="tag is-large is-link" href="/#display/two">two</a>
        <a class="tag is-large is-primary" href="/#display/three">three</a>
      </section>
    `
  }
}

customElements.define('main-application', MainApplication)
