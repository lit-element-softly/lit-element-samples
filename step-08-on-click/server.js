const http = require('http')
const url = require('url')
const fs = require('fs')
const path = require('path')
const port = process.argv[2] || 8080
const staticDirectory = process.argv[3] || `./`

http.createServer((req, res) => {
  console.log(`${req.method} ${req.url}`)
  // parse URL
  let extractedPathname = url.parse(req.url).pathname
  // extract URL path
  let pathname = `.${extractedPathname}` === staticDirectory ? `./index.html` : `.${extractedPathname}`
  // based on the URL path, extract the file extention
  let ext = path.parse(pathname).ext === "" ? ".html" : path.parse(pathname).ext
  // maps file extention to MIME
  const map = {
    '.ico': 'image/x-icon',
    '.html': 'text/html',
    '.js': 'text/javascript',
    '.json': 'application/json',
    '.css': 'text/css',
    '.png': 'image/png',
    '.jpg': 'image/jpeg',
    '.wav': 'audio/wav',
    '.mp3': 'audio/mpeg',
    '.svg': 'image/svg+xml',
    '.pdf': 'application/pdf',
    '.doc': 'application/msword'
  }

  fs.exists(pathname, (exist) => {
    if(!exist) {
      // if the file is not found, return 404
      res.statusCode = 404
      res.end(`File ${pathname} not found!`)
      return
    }

    // read file
    fs.readFile(pathname, (err, data) => {
      if(err){
        res.statusCode = 500;
        res.end(`Error getting the file: ${err}.`)
      } else {
        res.setHeader('Access-Control-Allow-Origin', '*')
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE')
        res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type')
        // Set to true if you need the website to include cookies in the requests sent
        res.setHeader('Access-Control-Allow-Credentials', true)
        res.setHeader('Content-type', map[ext] || 'text/plain' )
        res.end(data)
      }
    })
  })

}).listen(parseInt(port))

console.log(`🌍 Server is listening on port ${port}`)