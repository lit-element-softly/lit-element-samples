import { LitElement, html } from '../js/lit-element.js'

import {MyTitle} from './my-title.js'
import {MySubTitle} from './my-sub-title.js'
import {MyAmazingButton} from './my-amazing-button.js'

export class MainApplication extends LitElement {

  static get styles() { return [window.picnicCss, window.fineTuningCss] }

  render() {
    return html`

      <article class="card" style="padding: 10px;">
        <header>
          <my-title></my-title>
        </header>
        <my-sub-title my-text="I ❤️ LitElement"></my-sub-title>
      </article>

      <article class="card" style="padding: 10px;">
        <header>
          My Buttons
        </header>
        <my-amazing-button></my-amazing-button>
        <my-amazing-button></my-amazing-button>
      </article>
    `
  }
}

customElements.define('main-application', MainApplication)
