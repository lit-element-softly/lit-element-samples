import { LitElement, html } from '../js/lit-element.js'
import {MyTitle} from './my-title.js'
import {style} from './main-styles.js'

export class MainApplication extends LitElement {
  
  static get styles() { return [style] }

  render() {
    return html`
      <section class="container">
        <my-title></my-title>
      <section>     
    `
  }
}

customElements.define('main-application', MainApplication)
