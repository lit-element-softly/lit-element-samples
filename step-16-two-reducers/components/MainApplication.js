import { LitElement, html } from '../js/lit-element.js'
import { installRouter } from '../js/pwa-helpers.js'
import { store } from './store.js'

import { MessagesList } from './messages-list.js'

export class MainApplication extends LitElement {

  constructor() {
    super()
    window.store = store
    installRouter((location) => {
      console.log(location.hash)
    })
  }

  static get properties(){
    return {
      counter: Number
    }
  }

  // initialize at first change
  firstUpdated(changedProperties) {
    this.counter = store.getState().counter
    store.subscribe(() => {
      this.counter = store.getState().counter
    })
  }

  render() {
    return html`
      <div>
        <h1>🖖 live long and prosper 🌍</h1>
        <h1>
          ${this.counter}
        </h1> 
        <button @click="${this.onClickPlus}">😃 Increment</button>
        <button @click="${this.onClickMinus}">😠 Decrement</button>
        <hr>
        <messages-list></messages-list>
      </div>
    `
  }

  onClickPlus() {
    store.dispatch({ type: 'INCREMENT' })
  }

  onClickMinus() {
    store.dispatch({ type: 'DECREMENT' })
  }
}

customElements.define('main-application', MainApplication)
