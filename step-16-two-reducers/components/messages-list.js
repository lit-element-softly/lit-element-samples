import { LitElement, html } from '../js/lit-element.js'
import { installRouter } from '../js/pwa-helpers.js'
//import { messagesStore } from '../js/messages-store.js'

import { store } from './store.js'


export class MessagesList extends LitElement {

  static get properties(){
    return {
      counter: Number
    }
  }

  static get properties() {
    return {
      messages: { type: Array }
    }
  }
  
  constructor() {
    super()
    //window.messagesStore = store
    store.dispatch({ type: 'INIT' })
    this.messages = store.getState().messages
    console.log(this.messages)
    store.subscribe(() => {
      this.messages = store.getState().messages
    })
    /*
    this.messages = [
      {avatar: "🦊", name: "Foxy"},
      {avatar: "🦁", name: "Leo"},
      {avatar: "🐯", name: "Tigrou"}
    ]
    */
  }
  
  // initialize at first change
  firstUpdated(changedProperties) {
    
    //this.messages = messagesStore.getState()
    //console.log(this.messages)
    //messagesStore.subscribe(() => {
    //  this.messages = messagesStore.getState()
    //})
  }

  render() {
    return html`
      <table>
        <thead>
          <tr><th>message</th></tr>
        </thead>
        <tbody>
          ${this.messages.map(message => 
            html`
              <tr>
                <td>${message}</td>
              </tr>
            `
          )}
        </tbody>
      </table>
    `
  }

}

customElements.define('messages-list', MessagesList)
