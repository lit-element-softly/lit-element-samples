/*
  - https://www.smashingmagazine.com/2018/07/redux-designers-guide/
  - http://ramonvictor.github.io/tic-tac-toe-js/
*/

import { combineReducers, createStore } from '../js/redux.js'


// reducer
function counter(state = 0, action) {
  switch (action.type) {
    case 'INCREMENT':
      return state + 1
    case 'DECREMENT':
      return state - 1
    default:
      return state
  }
}

const messages = (state = [], action) => {
  switch(action.type) {
    case 'ADD_MESSAGE':
      return [...state, action.data]
      //return state.concat([action.data]);
    case 'INIT':
      return [...state, "good morning"]
      //return state.concat(["good morning"]);
    case 'REMOVE_MESSAGE':
      return state.filter(item => item !== action.data)
    default:
      return state;
  }
}

const reducer = combineReducers({ counter, messages })
// Create and export store so it can be imported and shared by app elements
export const store = createStore(reducer)

//https://redux.js.org/api/combinereducers
//https://redux.js.org/introduction/three-principles