import { LitElement, html } from '../js/lit-element.js'

import {MyTitle} from './my-title.js'
import {MySubTitle} from './my-sub-title.js'



export class MainApplication extends LitElement {

  static get styles() { return [window.picnicCss, window.fineTuningCss] }

  constructor() {
    super()
  }
  render() {
    return html`
      <section>
        <my-title></my-title>
        <my-sub-title my-text="I ❤️ LitElement ..."></my-sub-title>

      </section>

    `
  }
}

customElements.define('main-application', MainApplication)
