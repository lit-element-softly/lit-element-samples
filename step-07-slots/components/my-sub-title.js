import { LitElement, html } from '../js/lit-element.js'



export class MySubTitle extends LitElement {

  static get styles() { return [window.picnicCss, window.fineTuningCss] }

  static get properties() {
    return {
      myText: { attribute: 'my-text' }
    }
  }

  render() {
    return html`
      <h2 class="subtitle">
        ${this.myText}
      </h2>    
    `
  }

}
customElements.define('my-sub-title', MySubTitle)