import { LitElement, html } from '../js/lit-element.js'

export class MyTitle extends LitElement {

  render(){
    return html`
      <h1 class="title">
        Skeleton 🤖 [LitElement]
      </h1> 
    `
  }
}
customElements.define('my-title', MyTitle)