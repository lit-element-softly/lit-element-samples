import { LitElement, html } from '../js/lit-element.js'
import {MyTitle} from './my-title.js';

export class MainApplication extends LitElement {
  
  render() {
    return html`
      <section class="container">
        <div>
          <my-title></my-title>
        </div>
      <section>
    `
  }
}

customElements.define('main-application', MainApplication);
