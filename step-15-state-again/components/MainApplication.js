import { LitElement, html } from '../js/lit-element.js'
import { installRouter } from '../js/pwa-helpers.js'

export class MainApplication extends LitElement {

  constructor() {
    super()
    installRouter((location) => {
      console.log(location.hash)
    })
  }

  render() {
    return html`
      <div>
        <h1>🖖 live long and prosper 🌍</h1>
      </div>
    `
  }
}

customElements.define('main-application', MainApplication)