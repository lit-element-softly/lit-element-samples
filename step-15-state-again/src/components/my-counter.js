import { LitElement, html } from 'lit-element';
import {styles} from '../styles/styles.js';
import { store } from '../store.js';

export class MyCounter extends LitElement {

  static get properties(){
    return {
      counter: Number,
      message: String,
    }
  }

  render(){

    return html`
      ${styles}
      <h1 class="title is-1">
        ${this.message} ${this.counter}
      </h1> 
    `
  }

  firstUpdated(changedProperties) {
    this.counter = store.getState().counter
    this.message = store.getState().message
    store.subscribe(() => {
      this.counter = store.getState().counter
      this.message = store.getState().message
    })
  }


}
customElements.define('my-counter', MyCounter)