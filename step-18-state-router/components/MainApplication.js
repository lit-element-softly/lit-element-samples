import { LitElement, html } from '../js/lit-element.js'
import { installRouter } from '../js/pwa-helpers.js'

import { MessagesList } from './messages-list.js'

export class MainApplication extends LitElement {

  constructor() {
    super()
    installRouter((location) => {
      console.log(location.hash)
    })
  }

  render() {
    return html`
      <div>
        <h1>🖖 live long and prosper 🌍</h1>

        <messages-list></messages-list>
      </div>
    `
  }

}

customElements.define('main-application', MainApplication)
