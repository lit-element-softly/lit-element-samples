

import { createStore } from '../js/redux.js'

const messages = (state = [], action) => {
  switch(action.type) {
    case 'ADD_MESSAGE':
      return [...state, action.data]
      //return state.concat([action.data]);
    case 'INIT':
      return [...state, "good morning"]
      //return state.concat(["good morning"]);
    case 'REMOVE_MESSAGE':
      return state.filter(item => item !== action.data)
    default:
      return state;
  }
}

export const store = createStore(messages)

// ref : https://codeburst.io/redux-a-crud-example-abb834d763c9

// messagesStore.dispatch({ type: 'INIT' })
// messagesStore.dispatch({ type: 'ADD_MESSAGE', data:"hello..." })
// messagesStore.dispatch({ type: 'REMOVE_MESSAGE', data:"hello..." })

//this.buddies = [...this.buddies, buddy]

    // override the complete array instead of mutating it, 
    // otherwise the refresh will not be triggered.

// Combine Reducers
// https://redux.js.org/introduction/three-principles